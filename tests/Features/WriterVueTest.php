<?php

namespace Yormy\TranslationcaptainLaravel\Tests\Features;

use Yormy\TranslationcaptainLaravel\Services\FileWriters\WriterVue;
use Yormy\TranslationcaptainLaravel\Services\LabelPushService;
use Yormy\TranslationcaptainLaravel\Tests\TestCase;

class WriterVueTest extends WriterBaseTest
{
    public function setUp(): void
    {
        parent::setUp();

        $this->pathToOrchestra = 'vendor/orchestra/testbench-core/laravel';

        $push = new LabelPushService($this->locales);
        $allKeys = $push->getAllKeys();

        $this->translationsRead = $allKeys;

        $this->exportPath = '/tests/Features/Data/Exports/Vue/lang_vue';  // writing to orchestra location

        $writer = new WriterVue();
        $writer->setExportPath($this->exportPath);

        $writer->setLabels($this->translationsRead);
        $writer->export(0, $this->locales);
    }

    /** @test */
    public function vue_files_generated()
    {
        foreach ($this->locales as $locale) {
            foreach (array_keys($this->translationsRead[$locale]) as $file) {
                $filename = $this->generateFilename($locale, $file);
                if (false === strpos($filename, "___.json")) {
                    $this->assertFileExists($filename);
                }
            }
        }
    }

    /** @test */
    public function vue_files_contains_translation()
    {
        $filename = $this->generateFilename('en', 'action');
        $fileContents = $this->getFileContents($filename);

        $this->assertStringContainsString('"update.membership.account.level.gold"', $fileContents);
    }

    /** @test */
    public function vue_files_contains_translation_bindings()
    {
        $filename = $this->generateFilename('en', 'billing');
        $fileContents = $this->getFileContents($filename);

        $this->assertStringContainsString('The {_field_} field must be {width} pixels by {height} {height} pixels', $fileContents);
    }

    /** @test */
    public function blade_files_contains_translation_nested()
    {
        $filename = $this->generateFilename('en', 'yormy::level1/rr/messages');
        $fileContents = $this->getFileContents($filename);

        $this->assertStringContainsString('"The invite code {CODE} has expired."', $fileContents);
    }

    /**
     * ===================== HELPERS ====================
     */
    public function generateFilename(string $locale, string $file): string
    {
        if (!$this->isFileFromVendor($file)) {
            return $this->exportPath.
                DIRECTORY_SEPARATOR.
                $locale.
                DIRECTORY_SEPARATOR.
                $file.
                ".json";
        } else {
            return $this->exportPath.
                DIRECTORY_SEPARATOR.
                'vendor'.
                DIRECTORY_SEPARATOR.
                $this->getVendorName($file).
                DIRECTORY_SEPARATOR.
                $locale .
                DIRECTORY_SEPARATOR.
                $this->getFilenameWithoutVendor($file).
                ".json";
        }
    }

}
