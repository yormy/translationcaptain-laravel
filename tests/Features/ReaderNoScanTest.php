<?php

namespace Yormy\TranslationcaptainLaravel\Tests\Features;

use Illuminate\Support\Arr;

use Yormy\TranslationcaptainLaravel\Services\LabelPushService;
use Yormy\TranslationcaptainLaravel\Tests\TestCase;

class ReaderNoScanTest extends TestCase
{
    protected $translationsRead;

    protected $locales = ['nl','en'];

    public function setUp(): void
    {
        parent::setUp();

        config(['translationcaptain.source_code_scan.blade.enabled' => false]);

        $push = new LabelPushService($this->locales);
        $allKeys = $push->getAllKeys();

        $this->translationsRead = Arr::dot($allKeys);
    }

    /** @test */
    public function key_not_found_in_blade_when_scanning_disabled()
    {
        $this->assertArrayNotHasKeyLocales('app.welcome.found');
    }

    private function assertArrayNotHasKeyLocales($key, $locales = null)
    {
        if (! $locales) {
            $locales = $this->locales;
        }
        foreach ($locales as $locale) {
            $this->assertArrayNotHasKey("$locale.$key", $this->translationsRead);
        }
    }

}
