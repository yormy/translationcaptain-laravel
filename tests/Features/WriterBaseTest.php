<?php

namespace Yormy\TranslationcaptainLaravel\Tests\Features;

use Illuminate\Support\Facades\Storage;
use PHPUnit\Framework\Constraint\FileExists;
use Yormy\TranslationcaptainLaravel\Services\FileWriters\WriterBlade;
use Yormy\TranslationcaptainLaravel\Services\LabelPushService;
use Yormy\TranslationcaptainLaravel\Tests\TestCase;

abstract class WriterBaseTest extends TestCase
{
    protected array $translationsRead;

    protected array $locales = ['nl','en'];

    protected string $exportPath;

    protected string $pathToOrchestra;

    protected function getFileContents(string $filename): string
    {
        return file_get_contents($filename);
    }

    protected function assertFileExistsInOrchestra(string $filename): void
    {
        $this->assertFileExists($this->pathToOrchestra . $filename);
    }

    protected function isFileFromVendor($file)
    {
        return strpos($file, '::') !== false;
    }

    protected function getFilenameWithoutVendor($file)
    {
        $vendorSepPos = strpos($file, '::');
        return substr($file, $vendorSepPos + 2);
    }

    protected function getVendorName($file)
    {
        $vendorSepPos = strpos($file, '::');
        return substr($file, 0, $vendorSepPos);
    }
}
