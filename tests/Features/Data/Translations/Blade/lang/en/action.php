<?php

return [
    'update' => [
        'membership' => [
            'plan' => "monthly",
            'account' => [
                'level' => [
                    'gold' => "gold",
                ],
            ],
        ],
    ],
    'key_defined_in_blade_and_vue' => 'this key is defined in blade and vue with same translation',
];
