<?php

return [
    'update' => [
        'membership' => [
            'plan' => "maandelijks",
            'account' => [
                'level' => [
                    'gold' => "gound",
                ],
            ],
        ],
        'key_not_existing_in_english' => 'this key was only defined in dutch, no english key present'
    ],
];
