<?php

namespace Yormy\TranslationcaptainLaravel\Tests\Features;

use Illuminate\Support\Facades\Storage;
use PHPUnit\Framework\Constraint\FileExists;
use Yormy\TranslationcaptainLaravel\Services\FileWriters\WriterBlade;
use Yormy\TranslationcaptainLaravel\Services\LabelPushService;
use Yormy\TranslationcaptainLaravel\Tests\TestCase;

class WriterBladeTest extends WriterBaseTest
{
    public function setUp(): void
    {
        parent::setUp();

        $push = new LabelPushService($this->locales);
        $allKeys = $push->getAllKeys();

        $this->translationsRead = $allKeys;

        $this->exportPath = __DIR__ .'/Data/Exports/Blade/lang_blade';

        $writer = new WriterBlade();
        $writer->setExportPath($this->exportPath);

        $writer->setLabels($this->translationsRead);

        // TODO : backup generated test, rotating backup test
//        $disk = config('translationcaptain.backup.disk');
//        $path = config('translationcaptain.backup.path');
//        Storage::disk($disk)->deleteDirectory($path);

        $writer->export(0, $this->locales);

//        $newBackups = sizeof(Storage::disk($disk)->files($path));
//        $this->assertEquals(1 , $newBackups);

        $this->pathToOrchestra = 'vendor/orchestra/testbench-core/laravel/';
    }

    /** @test
     * @group now
     */
    public function blade_files_generated_plain_and_vendor()
    {
        foreach ($this->locales as $locale) {
            foreach (array_keys($this->translationsRead[$locale]) as $file) {
                $filename = $this->generateFilename($locale, $file);
                if (false === strpos($filename, "___.php")) {
                    $this->assertFileExists($filename);
                }
            }
        }
    }

    /** @test */
    public function blade_files_contains_translation()
    {
        $filename = $this->generateFilename('en', 'action');
        $fileContents = $this->getFileContents($filename);

        $this->assertStringContainsString('update.membership.account.level.gold', $fileContents);
        $this->assertStringContainsString('key_defined_in_blade_and_vue', $fileContents);
        $this->assertStringContainsString('this key is defined in blade and vue with same translation', $fileContents);
    }

    /** @test */
    public function blade_files_contains_translation_from_json()
    {
        $filename = $this->generateFilename('en', 'billing');
        $fileContents = $this->getFileContents($filename);

        echo $filename;

        $this->assertStringContainsString('plans.monthly.description', $fileContents);
        $this->assertStringContainsString('The :_field_ field must be :width pixels by :height :height pixels', $fileContents);
    }

    /** @test */
    public function blade_files_single_file_translation_generated()
    {
        foreach ($this->locales as $locale) {
            $filename = $this->generateFilenameSingleFileTranslation($locale);
            $this->assertFileExists($filename);
        }
    }

    /** @test */
    public function blade_files_single_file_translation_content()
    {
        $filename = $this->generateFilenameSingleFileTranslation('en');
        $fileContents = $this->getFileContents($filename);
        $this->assertStringContainsString('default_single_file_translations', $fileContents);
    }

    /** @test */
    public function blade_vendor_files_contains_translation()
    {
        $filename = $this->generateFilename('en', 'yormy::level1/rr/messages');
        $fileContents = $this->getFileContents($filename);

        $this->assertStringContainsString('expired.title', $fileContents);
        $this->assertStringContainsString('The invite code :CODE has expired.', $fileContents);
    }

    public function generateFilename(string $locale, string $file): string
    {
        if (strpos($file, '::') === false) {
            $filename = $this->exportPath.
                DIRECTORY_SEPARATOR .
                $locale .
                DIRECTORY_SEPARATOR .
                $file .
                ".php";
        } else {
            $filename = $this->exportPath.
                DIRECTORY_SEPARATOR.
                'vendor'.
                DIRECTORY_SEPARATOR.
                $this->getVendorName($file).
                DIRECTORY_SEPARATOR.
                $locale .
                DIRECTORY_SEPARATOR.
                $this->getFilenameWithoutVendor($file).
                ".php";
        }

        return $filename;
    }

    public function generateFilenameSingleFileTranslation(string $locale): string
    {
        return $this->exportPath.
            DIRECTORY_SEPARATOR.
            $locale.
            ".php";
    }
}
