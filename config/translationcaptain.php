<?php

use Yormy\TranslationcaptainLaravel\Services\FileReaders\ReaderBladePhp;
use Yormy\TranslationcaptainLaravel\Services\FileReaders\ReaderBladeJson;
use Yormy\TranslationcaptainLaravel\Services\FileReaders\ReaderVue;
use Yormy\TranslationcaptainLaravel\Services\FileWriters\WriterBlade;
use Yormy\TranslationcaptainLaravel\Services\FileWriters\WriterVue;

return [

    /*
    |--------------------------------------------------------------------------
    | Enabled or not
    |--------------------------------------------------------------------------
    | Enable or disable the entire TranslationCaptain functionality
    | When disabled, no keys are collected, no context is pushed,
    | no exception on missing keys
    |--------------------------------------------------------------------------
    |
    */
    'enabled' => true,

    'api_key' =>'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI1IiwianRpIjoiMzUwYjI4ZGFiMjU4MjM0Y2M5M2QwZDMwMmRmNDA2Y2QyMzhhYzVhNjEyMGM2ZWJmNzk0NmJmN2RhNTY2YjBkODk5OTg5MjgyNWZhMDE0ZjkiLCJpYXQiOjE2MjkxMDU5MjAuMjQzNDUsIm5iZiI6MTYyOTEwNTkyMC4yNDM0NTQsImV4cCI6MTY2MDY0MTkyMC4yMTc3NDYsInN1YiI6IjEwMDAiLCJzY29wZXMiOltdfQ.giiF0u3pqo6dZaaR9Z51FWG0UoKOIqPAVCoultr2GFg2P-TmUjKfNhHGAKyhsgu99x9msY2-SDvctOEJBGKdMm0AdmTVSc1LQyd24tqnQPr_zNnc3_l5n-gcPoNI41hjkRYhSXFLIT_5UP5YJWgv_tOnga3IH49ZISSzIt8eADt5FFWZzqc9fRxE4ygt7RQ8Zxq88JabUFvtzXYl0pWVrIzp2ga6y8A3mIcvv93xZbrsQU6T2TGLaQ6mvUMdYD2y5DYiI21pbDghLWp28PziPJ4amzwpnt284ZFJJanWXzjfQYmALy-n_SjPhSL8TUxuBvipiIVPjKXWbpOyYLZmKSJEEfDBdNmXtfiQLHNA-RcjTsm6zdDcJAr6oGb_4vJxJh9IjJQHvJ0ynac_SrKNPRSc06wWtSr1kLEvfZhF-Tr2PZ4HNtVJTBe0rm43nm5I6RuO1oHnJBDbMRIOQrizLP00W3P7BhM7uKoJUJvYi5Zqs-o_MgvwNhVxqdEnkYTo-L3I0MwY9NEQ-T_HLzCHxSaBlZDbvKDLH2d7cxMdpeyWO6cUsR9S7ptCCAtqMu1Ax-glwdD4kCXOmOUovgLscyn9dCGJFB8fgswVKynpVeJvDr5nOpEx4xhLg7J2ajeOxHo29vhIBDstN6PfwU5E6oY7vrjwN-XmKqvGypLNwQI',

    /*
    |--------------------------------------------------------------------------
    | Encryption
    |--------------------------------------------------------------------------
    | Data being transferred between TranslationCaptain and your application is encrypted
    | Specify the project encryption key that will be used for encryption
    | You can obtain this key from your project settings
    |--------------------------------------------------------------------------
    |
    */
    'encryption_key' => 'base64:KXn6nxC6UwsjT2TBpnkw0VLmih76GAPzR+wV36JGjWE=',
    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Translation captain project id
    |--------------------------------------------------------------------------
    | The id of the project on TranslationCaptain for which these keys are used
    */
    'project_id' => 'UGwzSrwXS3Oq4qwPUGwIIg',


    /*
    |--------------------------------------------------------------------------
    | TranslationCaptain push/pull/screenshot url
    |--------------------------------------------------------------------------
    | The destination of the TranslationCaptain api
    | 'url'=> 'https://translationcaptain.com/api/v1',
    |
    */
    'url'=> 'https://gd00gdpcr0.sharedwithexpose.com/api/v1',
    //https://.sharedwithexpose.com
    //'url' => 'http://localhost/api/v1', // 'https://backend.bedrock.local/
    //'url' => 'https://chatty.local/api/v1/multilingual', // cannot find this from within the same container


    /*
    |--------------------------------------------------------------------------
    | Group name of the key if there was no group name found
    | ie the translation of __('hello_world') does not have a group name.
    | To be able to store and handle this correctly a groupname must be present
    | Specify the default groupname here
    |--------------------------------------------------------------------------
    |
    */
    'group_when_group_missing' => '___',





    /*
    |--------------------------------------------------------------------------
    | Default Locale
    |--------------------------------------------------------------------------
    | This is de default base locale. New translation lines stored in the database will
    | automatically get this language as their base language. In general this is best
    | to keep this in english (en)
    | Base language means the language that is used to translate into other languages
    | Example: if the base language is set to 'en', then all translations will be based on the english text.
    | Meaning your translators see the english text and need to translate it into french
    | if the base language is set to 'de' then all translations will be based on german.
    | Meaning your translators see the german text and need to translate it into french
    | STRONGLY RECOMMENDED to leave this in english, as most translators speak english
    |
    */
    'default_locale' => env('DEFAULT_LOCALE', 'en'),


    /*
    |--------------------------------------------------------------------------
    | Supported Locales
    |--------------------------------------------------------------------------
    | The languages you want to support. Languages not listed here are simply ignored from pushing and pulling
    | ie ['en', 'nl', 'de']
    |
    */
    'locales' => ['en', 'nl', 'de'],


    /*
    |--------------------------------------------------------------------------
    | Models
    |--------------------------------------------------------------------------
    | Define models you want to sync with TranslationCaption
    | These models need the TranslationCaptain Traits
    |
    */
    'models' => [
        'marketingsnippit' => [
            'class' => \Mexion\BedrockCore\Models\MarketingSnippit::class,
            'translationcaptain_id' => 'e7h6tLVnQFWNIZK2OsXBvg',
            'id_field' => 'id'
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Readers
    |--------------------------------------------------------------------------
    | Define the source paths and type of readers you want to use
    |
    */
    'readers' => [
        [
            'import_path' => '/resources/lang',
            'reader' => ReaderBladeJson::class,
        ],
        [
            'import_path' => '/resources/lang',
            'reader' => ReaderBladePhp::class,
        ],
        [
            'import_path' => '/resources/js/lang',
            'reader' => ReaderVue::class,
        ],

    ],


    /*
    |--------------------------------------------------------------------------
    | Writers
    |--------------------------------------------------------------------------
    | Define the destination paths and the service to use to write
    |
    */
    'writers' => [
        [
            'export_path' => '/resources/lang',
            'writer' => WriterBlade::class,
        ],
        [
            'export_path' => '/resources/js/lang',
            'writer' => WriterVue::class,
        ],
    ],


    /*
    |--------------------------------------------------------------------------
    | Source files
    |--------------------------------------------------------------------------
    | Where your source files are located to search through to find extra undefined keys
    |
    */
    'source_code_scan' => [
        'blade' => [
            'enabled' => true,
            'paths' => [
                '/resources/views'
                //..more paths to blade files to be scanned
            ],
        ],
        'vue' => [
            'enabled' => false,
            'paths' => [
                '/resources/0_archive/js/components'
                //..more paths to vue files to be scanned
            ]
        ],
    ],


    /*
    |--------------------------------------------------------------------------
    | Collect screenshots
    |--------------------------------------------------------------------------
    */
    'screenshot' => [

        /*
        |--------------------------------------------------------------------------
        | Collect screenshot for which items
        |--------------------------------------------------------------------------
        | Collect the context for which items, All items, or only newly added items
        */
        'collect' => "ALL",  // ALL || NEW


        /*
        |--------------------------------------------------------------------------
        | Collect screenshots
        |--------------------------------------------------------------------------
        | When to make the actual screenshots.
        | Always, never or only when there is a cookie with the name as defined in 'enabled_cookie' is present
        */
        'trigger' => 'ON_ENABLED_COOKIE', // ALWAYS | ON_ENABLED_COOKIE | NONE

        /*
        |--------------------------------------------------------------------------
        | Enabled
        |--------------------------------------------------------------------------
        | When the collect context is set to ON_ENABLED_COOKIE, this is the name of the cookie it checks.
        | If this cookie is set then the system collects the data, if cookie is not set the collection
        | is skipped
        | NOTE : This cookie needs to be unencrypted (place in your EncryptCookies.php except list)
        */
        "enabled_cookie" => "translationcaptain",


        /*
        |--------------------------------------------------------------------------
        | Collect storage
        |--------------------------------------------------------------------------
        | The name of the cookie to remember which keys are on this page for associating the screenshot to.
        | This will contain an array of keynames that will be associated to the captured screenshot
        | NOTE : This cookie needs to be unencrypted (place in your EncryptCookies.php except list)
        */
        "collect_cookie" => "translationcaptain_context",


        /*
        |--------------------------------------------------------------------------
        | Exclusions
        |--------------------------------------------------------------------------
        | List the URLS, Routes or Keys you want to exclude from screenshotting
        |
        */
        'exclude' => [

            "urls" => [
                "/home/text/exclued"
            ],

            "routes" => [
                "user.home1"
            ],

            "keys" => [
                "app.language"
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Invalid Key
    |--------------------------------------------------------------------------
    */
    'invalid_key' => [
        'exception' => false,
        'autofix_postfix' => "#MODIFIED",
    ],

    /*
    |--------------------------------------------------------------------------
    | Missing Translations
    |--------------------------------------------------------------------------
    */
    'missing_translation' => [
        /*
        |--------------------------------------------------------------------------
        | Throw exception when missing key found
        | When during the running of the application code is executed which does not have a translation
        | Then an exception is thrown
        |--------------------------------------------------------------------------
        |
        */
        'exception' => false,

        'log' => env('TRANS_LOG_MISSING', true),


        /*
        |--------------------------------------------------------------------------
        | Missing keys can be marked in the ui as missing by adding a prefix
        | ie When the key __('missing-key') is renedered to the screen with a prefix of '#'
        | then you will see '#missing-key' on the display to remind the developer to add this key
        |--------------------------------------------------------------------------
        |
        */
        'display_prefix' => '#',

        /*
        |--------------------------------------------------------------------------
        | Queue filename
        | the actual filename that is uses to store new found keys before
        | uploading them. This is just a temporary file
        | and it will be deleted when the keys are pushed
        |--------------------------------------------------------------------------
        |
        */
        'queue_filename' => 'translationcaption-queue.log',
    ],


    /*
    |--------------------------------------------------------------------------
    | Backup
    |--------------------------------------------------------------------------
    | Translation files will be backed up. Define your backup strategy
    | This will auto rotate your backups and only keep the set number of backups
    | ie
    |   disk = 'local'
    |   path = 'backup/tc'
    |   keep_number_backups = 5
    |
    |--------------------------------------------------------------------------
    |
    */
    'backup' => [
        'enabled' => true,
        'disk' => 'local',
        'path' => 'backup/tc',
        'keep_number_backups' => 4
    ],

    /*
    |--------------------------------------------------------------------------
    | Databinding (do not change)
    |--------------------------------------------------------------------------
    | Specify the start and end tokens of the databinding values.
    | TranslationCaptain needs to know how to recognize text in the translations that will be bound to a value later
    | standard laravel: 'You have purchased :itemcount items'.
    | This can be translated into 'Je hebt :itemcount dingen gekocht.
    | Itemcount cannot be translated as this is the value from the code. Translationcaptain needs to know this.
    | Any recognized databinding in vue and laravel are transformed for Translationcaptain into something like
    | 'You purchased %%itemcount%% products
    |--------------------------------------------------------------------------
    |
    */
    'databinding' => [
        'start' => "%%",
        'end' => "%%",
    ],

    /*
    |--------------------------------------------------------------------------
    | Logging or not
    |--------------------------------------------------------------------------
    | Enable or disable logging of your pulls and pushes to TranslationCaptain
    |--------------------------------------------------------------------------
    |
    */
    'logging' => true,
];
