#Namespaces

## Non Namespaced
Laravel has default translations without a namespace ```__('example)```. These non-namespaced will be grouped into 
a dummy namespace of tripple underscore (```___```). 
You can change this grouping into anything you want in the config but beware of clashes

## Vendor Namespaced
Packages have their own namespaced translation keys and can be overwritten in your app like ```__('package::hello_world')```


# Missing Translation Handling
When a key is found, but no translation has been found in the translation files, then key is still added to translationcaptain.
In that case the translation will be the keyname prefixed with a #
ie:
```__('filename.non-existing-key)```
will be stored with a default translation into
```#filename.non-existing-key```

or for non namespaced
ie:
```__('example)```
will be stored with a default translation into
```#___.non-existing-key```


# On to many keys

## Pushing new keys
TranslationCaptain allows a certain number of keys. When you upload more keys into your account than you plan allows the following happens
1) An event is fired ```PushFailedEvent``` which you can handle
2) And entry in the debug log is made
3) An email is sent to you 

## Pulling translations
When your account has maxed it allowed number of keys, pushing new keys is no longer allowed, this means that your keys in TranslationCaptain no longer match
with the keys in your project code.
Hence when pulling all the keys from TranslationCaptain and overwriting your local translation files would result in dataloss
Keys that were in your local files but not yet uploaded to TranslationCaptain would be lost.
To prevent that we block pulling keys if you maxed out your number of keys
When you still pull keys the following happens
1) An event is fired ```PullFailedEvent``` which you can handle
2) And entry in the debug log is made
3) An email is sent to you 


# Models
Model pushing and deleting works with Eloquent Model Events, when no model-events are fired, TranslationCaptain will not be updated
Model events are not fired on bulk changes, see eloquent details on when model events are not fired
ie Model::all()->delete() is a bulk change which does not fire events
you need to refactor that to loop through all models and call the delete method on each and every one of them.


# defining model
Model fields-name must be exactly as your record names
