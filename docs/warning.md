# Warning
When an admin pulls the latest translations to a live environment, the files on the live environment will be changed
This then results in a remote filesystem which has newer files than your source control
This might result in pull conflicts unless you prepare for that in your deploy script
