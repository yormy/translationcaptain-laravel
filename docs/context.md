# blade
Create a blade file 
```
_translationcaptain.blade.php
```

```
<div style="background-color:orangered;z-index:999;position: fixed;bottom: 0;right: 0;">TC Loaded</div>
@if (config('translationcaptain.enabled') && Cookie::get(config('translationcaptain.screenshot.enabled_cookie')))
    <div style="background-color:orangered;z-index:999;position: fixed;bottom: 0;right: 0;">TC Running</div>
    <script>
        const projectId='{{config('translationcaptain.project_id')}}';

        function getCurrentLanguage()
        {
            return 'en';
        }

        function getCookieKey()
        {
            return '{{config('translationcaptain.screenshot.collect_cookie')}}';
        }

        function getPostContextUrl()
        {
            return '{{config('translationcaptain.url')}}';
        }
    </script>
    <script  type="text/javascript" src="https://biomeaning.local/js/tc/context/index.js">
    </script>
@endif
```

# include this file in the layouts
Include this file in the body of your app.blade.
This way the translationcaptain context will be used on every page that is based on this app.blade

Or include this file in those files that you want to use tc for

# Content Security Policy 
If you have Content Security Policy enabled on your server have to allow connections with translationcaptain


# Enabling
Context screenshots are only uploaded when
- Your plan allows for contexts
- You have the proper setup (see above)
- Your config enables TranslationCaptain
- There is a cookie to enable grabbing the screenshot

# dealing with the cookie
- Easiest is to create a route that sets the cookie so that after 
visiting other pages the cookie is still there and the screenshots are pushed
- so by deafault the context is not pushed, unless the cookie is set. 
This prevents delays during development if you are not interested in pushing the new screenshots every time
