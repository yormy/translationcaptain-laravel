# Concept
1) Collect all keys form your application
2) Push all keys with the current existing translation to translationcaptain
3) Translate within translationcaptain portal
4) Pull translations into your application

Pushing and pulling of keys can be done from the command line of your application or 
you can publish a route so that non-developers can push-pull keys to update translations.



This package is to push and pull your translations from translationcaptain
Push
1) it will collect all translatable keys from your language files
2) it can scan your blade files to collect all translatable keys

Pull
....

Maintain file structure
dottable
Vendor Namespacing

# Non-destructive
During uploading of keys / translations the database is only appended to, 
when keys are missing in your upload they will remain in the database

When you change the translation in the filesystem and upload the keys again, the database will not reflect your filesystem changes
This is to maintain the single source of truth in the database.
So always change your translations in the database
Only the initial key translation is uploaded on key creation


# Documentation

Push all keys to TranslationCaptain
Pull all keys and translation from TranslationCaptain
Generate language files in laravel php format from the pull of TranslationCaptain
Upload a screenshot of the usages of keys to TranslationCaptain

# Setup
in _translationcaptain.blade.php
you need to implement the getLanguage function to return the 2 letter language code of the current language of your view
this will be used to store teh language with the screenshot





# Adding a key
1) Add the key to your code, run 
Sync (is the same as push and then pull)
   
2) Add the key to any language file
Sync (is the same as push and then pull)

When a new key is being displayed (parsed by the translator), it is also logged in the queue for uploading on the next push

# Translations
All translations happen in TranslationCaptain and on the next pull the local translation files are overwritten with the newly pulled files

## Prinicples


## Changes
* [TODO ITEMS](todo.md)
* [CHANGELOG](../CHANGELOG.md)

