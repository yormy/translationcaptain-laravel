## Publish and customize the config file
todo

### Specify the location of your blade files
Blade files can be scanned to find unpublished translatable keys. 
Specify the root location of your blade files
```
'source_code_scan_paths' => [
    'blade' => [
        '/resources/views/'
    ]
],
```




## Adding routes
Add the following to your RoutesServiceProvider.php

needs WEB middleware
```
Route::middleware('web')
    ->group(function () {
        Route::Translationcaptain();
    });
```

This will publish 2 routes
* translationcaptain/push (translationcaptain.push)
* translationcaptain/pull (translationcaptain.pull)

### If you want to use other prefixes 
```
Route::middleware('web')
    ->group(function () {
        Route::Translationcaptain('example');
    });
```
This will publish 2 routes
* example/translationcaptain/push (example.translationcaptain.push)
* example/translationcaptain/pull (example.translationcaptain.pull)
