# Pushing data
The first time you push your existing data it might take quite a long time depending on the number of items you are pushing


# Caching
After your first pull we store the keys on your machine, so all subsequent pushes will only push new keys/models. This will speed up your pushes tremendously

## Warning
- we only push NEW keys, meaning if you change the translation of a key, this translation is not pushed
- This also allows for proper data consistency, in a way that the data in TranslationCaptain is never accidentally destroyed when you do a push


# Force push
When force pushing all keys will be pushed (takes a long time), and the translations of the keys will overwrite what is currenty in TranslationCaption

# Cache deletion
When you delete the cache, all keys will be pushed, but the translations ????
will or not overwrite ? not is better, leave that to the force push

