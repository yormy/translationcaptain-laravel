<?php

namespace Yormy\TranslationcaptainLaravel\Exceptions;

use Exception;

class InvalidOutputException extends Exception
{
    /**
     * InvalidValueException constructor.
     */
    public function __construct(string $filename)
    {
        parent::__construct("The output file is invalid: $filename. File has not been updated");
    }
}
