<?php

namespace Yormy\TranslationcaptainLaravel\Exceptions;

use Exception;

class InvalidModelPullException extends Exception
{
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}
