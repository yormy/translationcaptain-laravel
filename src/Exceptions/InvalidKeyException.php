<?php

namespace Yormy\TranslationcaptainLaravel\Exceptions;

use Exception;

class InvalidKeyException extends Exception
{
    /**
     * InvalidValueException constructor.
     */
    public function __construct(string $key)
    {
        parent::__construct("This is an invalid key '$key' Keys cannot contain periods (.)");
    }
}
