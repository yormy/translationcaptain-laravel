<?php

namespace Yormy\TranslationcaptainLaravel\Exceptions;

use Exception;

class InvalidSetupException extends Exception
{
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}
