<?php

namespace Yormy\TranslationcaptainLaravel\Exceptions;

use Exception;

class DuplicateKeyException extends Exception
{
    /**
     * InvalidValueException constructor.
     */
    public function __construct(string $key, string $valueOrigin, string $valuetoMerge)
    {
        parent::__construct("Duplicate language key with different translation found for key='$key' original value='$valueOrigin' wanted to merge='$valuetoMerge'.
        Best is to remove the wrong one. Your best bet is to remove old not overwritten language files after your pulled");
    }
}
