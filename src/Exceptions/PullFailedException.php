<?php

namespace Yormy\TranslationcaptainLaravel\Exceptions;

use Exception;

class PullFailedException extends Exception
{
    /**
     * InvalidValueException constructor.
     */
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}
