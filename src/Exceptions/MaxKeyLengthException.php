<?php

namespace Yormy\TranslationcaptainLaravel\Exceptions;

use Exception;

class MaxKeyLengthException extends Exception
{
    /**
     * InvalidValueException constructor.
     */
    public function __construct(string $key)
    {
        parent::__construct("The specified key is too long ($key). Max characters is 150");
    }
}
