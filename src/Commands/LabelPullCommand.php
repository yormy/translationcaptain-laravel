<?php

namespace Yormy\TranslationcaptainLaravel\Commands;

use Illuminate\Console\Command;
use Yormy\TranslationcaptainLaravel\Commands\Traits\LabelPullTrait;

class LabelPullCommand extends Command
{
    use LabelPullTrait;

    public $signature = 'translationcaptain:pull-labels {option?}';

    public $description = 'Pull translations from TranslationCaptain to refresh local files';

    public function handle()
    {
        $this->goPull();
    }
}
