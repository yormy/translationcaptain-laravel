<?php

namespace Yormy\TranslationcaptainLaravel\Commands;

use Illuminate\Console\Command;
use Yormy\TranslationcaptainLaravel\Commands\Traits\LabelPullTrait;
use Yormy\TranslationcaptainLaravel\Commands\Traits\LabelPushTrait;

class SyncCommand extends Command
{
    use LabelPushTrait;
    use LabelPullTrait;

    public $signature = 'translationcaptain:sync {option?}';

    public $description = 'Push changes to TranslationCaptain and Pull to refresh local files
    "translationcaptain:sync details" : print out the processed keys';

    public function handle()
    {
        $this->goPush();

        $this->comment('   ');

        $this->goPull();
    }
}
