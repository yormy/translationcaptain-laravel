<?php

namespace Yormy\TranslationcaptainLaravel\Commands;

use Illuminate\Console\Command;
use Yormy\TranslationcaptainLaravel\Services\ModelPushService;
use Yormy\TranslationcaptainLaravel\Traits\ModelValidationTrait;

class ModelPushAllCommand extends Command
{
    use ModelValidationTrait;

    public $signature = 'translationcaptain:push-models-all';

    public $description = 'Push all models with all languages to TranslationCaptain';

    public function handle()
    {
        $line = "-------------------------------------------------------------------------------------";

        $this->comment($line);
        $this->comment('TranslationCaptain: Upload All Language');
        $this->comment("You are about to upload your models. This will overwrite the values in TranslationCaptain.");
        $this->comment("Translations that are not in your own database will be lost.");
        $this->comment($line);

        if(!$this->confirm('Do you wish to continue? (yes|no)[no]',false))
        {
            $this->comment('Ahoy Captain.. we\'ve abanded ship. Nothing has been done');
        }

        $this->goPush();


    }

    public function goPush()
    {
        $this->comment('Pushing all models to TranslationCaptain');
        $this->comment('Pushing...');


        $modelPushService = new ModelPushService();
        $data = $modelPushService->buildData();

        $response = $modelPushService->pushAllModelsToRemote($data);

        $this->comment('');
        $this->comment('Ahoy Captain.. we\'re done');
        if (array_key_exists('data', $response)) {
            $data = $response['data'];

            if (array_key_exists('created', $data)) {
                $this->comment('    ' . 'Created models : ' . $data['created']);
            }
            if (array_key_exists('updated', $data)) {
                $this->comment('    ' . 'Updated models : ' . $data['updated']);
            }
        }
    }
}
