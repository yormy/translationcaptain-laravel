<?php

namespace Yormy\TranslationcaptainLaravel\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Yormy\TranslationcaptainLaravel\Commands\Traits\LabelPullTrait;
use Yormy\TranslationcaptainLaravel\Services\LabelPullService;
use Yormy\TranslationcaptainLaravel\Services\ModelPullService;

class ModelPullCommand extends Command
{
    public $signature = 'translationcaptain:pull-models {option?}';

    public $description = 'Pull model translations from TranslationCaptain';

    public function handle()
    {
        $this->goPull();
    }

    public function goPull()
    {
        $this->comment('All hands hoay, we\'re getting the model translations from TranslationCaptain');
        $this->comment('Getting...');

        $pull = new ModelPullService();
        $pulledModels = $pull->pullFromRemote();

        $option = $this->argument('option');
        if ($option === 'details') {
            foreach ($pulledModels as $key => $values) {
                $this->comment('------------------------------------');
                $this->comment('   ' . $key);

                foreach ($values as $fieldName => $languages) {
                    $this->comment('       ' . $fieldName . '='. json_encode($languages));
                }
            }
        }

        $this->comment("");

        $this->comment('Captain, we pulled '. count($pulledModels). ' models');
        $this->comment('Ahoy Captain.. we\'re done');
    }
}
