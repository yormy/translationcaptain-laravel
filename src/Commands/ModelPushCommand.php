<?php

namespace Yormy\TranslationcaptainLaravel\Commands;

use Illuminate\Console\Command;
use Yormy\TranslationcaptainLaravel\Services\ModelPushService;
use Yormy\TranslationcaptainLaravel\Traits\ModelValidationTrait;

class ModelPushCommand extends Command
{
    use ModelValidationTrait;

    public $signature = 'translationcaptain:push-models';

    public $description = 'Push all models, english only to TranslationCaptain';

    public function handle()
    {
        $line = "-------------------------------------------------------------------------------------";

        $this->comment($line);
        $this->comment('TranslationCaptain: Upload English Language only');
        $this->comment("You are about to upload your models. This will overwrite the english values in TranslationCaptain.");
        $this->comment("Other languages in TranslationCaptain will not be touched");
        $this->comment($line);

        if(!$this->confirm('Do you wish to continue? (yes|no)[no]',false))
        {
            $this->comment('Ahoy Captain.. we\'ve abanded ship. Nothing has been done');
        }

        $this->goPush();
    }

    public function goPush()
    {
        $i18n = 'en';

        $this->comment('Pushing all models english to TranslationCaptain');
        $this->comment('Pushing...');

        $modelPushService = new ModelPushService();
        $data = $modelPushService->buildData($i18n);

        $response = $modelPushService->pushAllModelsToRemote($data, $i18n);

        $this->comment('');
        $this->comment('Ahoy Captain.. we\'re done');
        if (array_key_exists('data', $response)) {
            $data = $response['data'];

            if (array_key_exists('created', $data)) {
                $this->comment('    ' . 'Created models : ' . $data['created']);
            }
            if (array_key_exists('updated', $data)) {
                $this->comment('    ' . 'Updated models : ' . $data['updated']);
            }
        }
    }

}
