<?php

namespace Yormy\TranslationcaptainLaravel\Commands;

use Illuminate\Console\Command;
use Yormy\TranslationcaptainLaravel\Commands\Traits\LabelPushTrait;

class LabelPushCommand extends Command
{
    use LabelPushTrait;

    public $signature = 'translationcaptain:push-labels {option?}';

    public $description = 'Push local changes and found keys to TranslationCaptain
    "translationcaptain:push details" : print out the processed keys';

    public function handle()
    {
        $this->goPush();
    }
}
