<?php

namespace Yormy\TranslationcaptainLaravel\Http\Controllers;

use Illuminate\Routing\Controller;
use Yormy\TranslationcaptainLaravel\Services\ModelPullService;

class ModelPullController extends Controller
{
    public function pull()
    {
        $pull = new ModelPullService();
        $pulledModels = $pull->pullFromRemote();

        echo json_encode($pulledModels);
    }
}
