<?php

namespace Yormy\TranslationcaptainLaravel\Http\Controllers;

use Illuminate\Routing\Controller;
use Yormy\TranslationcaptainLaravel\Services\LabelPullService;
use Yormy\TranslationcaptainLaravel\Services\ModelPullService;

class LabelPullController extends Controller
{
    public function pull()
    {
        $pull = new LabelPullService();
        $result = $pull->pullFromRemote();
        echo json_encode($result);
    }
}
