<?php

namespace Yormy\TranslationcaptainLaravel\Http\Controllers;

use Illuminate\Routing\Controller;
use Yormy\TranslationcaptainLaravel\Exceptions\InvalidSetupException;
use Yormy\TranslationcaptainLaravel\Services\ModelPushService;

class ModelPushController extends Controller
{
    public function push(string $i18n = null)
    {
        if ($i18n && !in_array($i18n, config('translationcaptain.locales'))) {
            $message = "Language $i18n is not supported";
            throw new InvalidSetupException($message);
        }

        $modelPushService = new ModelPushService();
        $data = $modelPushService->buildData($i18n);

        return $modelPushService->pushAllModelsToRemote($data, $i18n);
    }
}
