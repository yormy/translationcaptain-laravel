<?php

namespace Yormy\TranslationcaptainLaravel\Http\Controllers;

use Illuminate\Routing\Controller;
use Yormy\TranslationcaptainLaravel\Services\LabelPushService;

class LabelPushController extends Controller
{
    public function push()
    {
        $locales = config('translationcaptain.locales');
        $push = new LabelPushService($locales);

        return $push->pushLabelsToRemote();
    }
}
