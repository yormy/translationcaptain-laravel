<?php

namespace  Yormy\TranslationcaptainLaravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class LogProjectPushpullModel extends Model
{
    protected $table = 'log_translationcaptain_pushpull_models';

    protected $fillable = [
        'project_id',
        'data',
        'result',
        'pushed',
        'pulled',
        'user_id'
    ];

    protected static function boot(): void
    {
        parent::boot();

        static::saving(function ($model) {
            $model->user_id = 0;
            if ($user = Auth::user()) {
                $model->user_id = $user->id;
            }
        });
    }

}
