<?php

namespace Yormy\TranslationcaptainLaravel\Repositories;

use Mexion\BedrockCore\Repositories\BaseRepository;
use Mexion\BedrockUsers\Models\Member;
use Mexion\MultilingualAdmin\Models\LogProjectPushpulls;
use Mexion\MultilingualAdmin\Models\Project;
use Yormy\TranslationcaptainLaravel\Models\LogProjectPushpull;

class LogProjectPushPullRepository
{
    public function addPush(string $project_id, string $type, array $data, int $count, array $result = [])
    {
        if (!config('translationcaptain.logging')) {
            return;
        }

        LogProjectPushpull::create([
            'project_id' => $project_id,
            'type' => $type,
            'data' => json_encode($data),
            'result' => json_encode($result),
            'pushed' => $count,
        ]);
    }

    public function addPull(string $project_id, string $type, array $data, int $count, array $result = [])
    {
        if (!config('translationcaptain.logging')) {
            return;
        }

        LogProjectPushpull::create([
            'project_id' => $project_id,
            'type' => $type,
            'data' => json_encode($data),
            'result' => json_encode($result),
            'pulled' => $count,
        ]);
    }
}
