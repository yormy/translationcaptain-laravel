<?php

namespace Yormy\TranslationcaptainLaravel\Repositories;

use Mexion\BedrockCore\Repositories\BaseRepository;
use Mexion\BedrockUsers\Models\Member;
use Mexion\MultilingualAdmin\Models\LogProjectPushpulls;
use Mexion\MultilingualAdmin\Models\Project;
use Yormy\TranslationcaptainLaravel\Models\LogProjectPushpullModel;

class LogProjectPushPullModelRepository
{
    public function addPush(string $project_id, array $data, int $count, array $result = [])
    {
        if (!config('translationcaptain.logging')) {
            return;
        }

        LogProjectPushpullModel::create([
            'project_id' => $project_id,
            'data' => json_encode($data),
            'result' => json_encode($result),
            'pushed' => $count,
        ]);
    }

    public function addPull(string $project_id, array $data, int $count, array $result = [])
    {
        if (!config('translationcaptain.logging')) {
            return;
        }

        LogProjectPushpullModel::create([
            'project_id' => $project_id,
            'data' => json_encode($data),
            'result' => json_encode($result),
            'pulled' => $count,
        ]);
    }
}
