<?php

namespace Yormy\TranslationcaptainLaravel;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Yormy\TranslationcaptainLaravel\Commands\LabelPullCommand;
use Yormy\TranslationcaptainLaravel\Commands\LabelPushCommand;
use Yormy\TranslationcaptainLaravel\Commands\ModelPullCommand;
use Yormy\TranslationcaptainLaravel\Commands\ModelPushAllCommand;
use Yormy\TranslationcaptainLaravel\Commands\ModelPushCommand;
use Yormy\TranslationcaptainLaravel\Commands\SyncCommand;
use Yormy\TranslationcaptainLaravel\Exceptions\InvalidSetupException;
use Yormy\TranslationcaptainLaravel\Http\Controllers\LabelPullController;
use Yormy\TranslationcaptainLaravel\Http\Controllers\LabelPushController;
use Yormy\TranslationcaptainLaravel\Http\Controllers\ModelPullController;
use Yormy\TranslationcaptainLaravel\Http\Controllers\ModelPushController;
use Yormy\TranslationcaptainLaravel\Providers\EventServiceProvider;
use Yormy\TranslationcaptainLaravel\Providers\TranslationServiceProvider;

class TranslationcaptainLaravelServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/translationcaptain.php' => config_path('translationcaptain.php'),
            ], 'config');

            $this->publishes([
                __DIR__ . '/../resources/views/blade' => base_path('resources/views/vendor/translationcaptain'),
            ], 'blade');

            $this->publishes([
                __DIR__ . '/../resources/views/vue' => base_path('resources/views/vendor/translationcaptain'),
                __DIR__ . '/../resources/assets' => resource_path('assets/vendor/translationcaptain'),
            ], 'vue');


            $this->commands([
                LabelPushCommand::class,
                LabelPullCommand::class,
                ModelPullCommand::class,
                ModelPushCommand::class,
                ModelPushAllCommand::class,
                SyncCommand::class,
            ]);

            $this->loadMigrationsFrom(__DIR__ . '/Database/Migrations');

            $ui_type = 'blade'; // todo
        } else {
            $ui_type = 'blade';
            if ("VUE" === config('translationcaptain.ui_type')) {
                $ui_type = 'vue';
            }
        }

        $this->loadViewsFrom(__DIR__ . '/../resources/views/'. $ui_type, 'translationcaptain');

        $this->registerGuestRoutes();

        $this->app->make('config')->set('logging.channels.translationcaptain', [
            'driver' => 'daily',
            'path' => storage_path('logs/translationcaptain.log'),
            'level' => 'debug',
            'days' => 31,
        ]);

        $this->validateConfig();
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/translationcaptain.php', 'translationcaptain');
        $this->app->register(EventServiceProvider::class);
        $this->app->register(TranslationServiceProvider::class);
    }

    private function validateConfig()
    {
        $locales = config('translationcaptain.locales');
        if (!$locales || !is_array($locales)) {
            throw new InvalidSetupException('Config has no `locales` array, setup the iso2 languages you want to support');
        }

        $url = config('translationcaptain.url');
        if (!$url) {
            throw new InvalidSetupException('Config has no `url`, setup the url to: translationcaptain.com');
        }

        $url = config('translationcaptain.project_id');
        if (!$url) {
            throw new InvalidSetupException('Config has no `project_id`, copy the id of the project from Translation-Captain into your config.');
        }

        $url = config('translationcaptain.api_key');
        if (!$url) {
            throw new InvalidSetupException('Config has no `api_key`, copy the api_key of your Translation-Captain account into your config.');
        }

    }

    private function registerGuestRoutes()
    {
        Route::macro('Translationcaptain', function (string $prefix = '') {
            Route::prefix($prefix)->name($prefix ? $prefix . "." : '')->group(function () {
                Route::get('translationcaptain/labels-push', [LabelPushController::class, 'push'])
                    ->name('translationcaptain.labels-push');
                Route::get('translationcaptain/labels-pull', [LabelPullController::class, 'pull'])
                    ->name('translationcaptain.labels-pull');


                Route::get('translationcaptain/models-push/{i18n?}', [ModelPushController::class, 'push'])
                    ->name('translationcaptain.models.push');
                Route::get('translationcaptain/models-pull', [ModelPullController::class, 'pull'])
                    ->name('translationcaptain.models.pull');
            });
        });
    }

}
