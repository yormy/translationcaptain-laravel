<?php declare(strict_types=1);

namespace Yormy\TranslationcaptainLaravel\Services;

use Illuminate\Support\Facades\Http;
use Yormy\TranslationcaptainLaravel\Exceptions\InvalidModelPullException;
use Yormy\TranslationcaptainLaravel\Exceptions\ModelPullFailedFieldException;
use Yormy\TranslationcaptainLaravel\Exceptions\PullFailedException;
use Yormy\TranslationcaptainLaravel\Traits\hasAttributeTrait;
use Yormy\TranslationcaptainLaravel\Traits\ModelValidationTrait;
use Yormy\TranslationcaptainLaravel\Traits\ProjectPushPullTrait;

class ModelPullService
{
    use ModelValidationTrait;
    use hasAttributeTrait;
    use ProjectPushPullTrait;

    private array $return = [];

    protected $encryptService;

    public function __construct()
    {
        $this->encryptService = new EncryptService(config('translationcaptain.encryption_key'));
    }

    public function pullFromRemote()
    {
        $configuredModels = config('translationcaptain.models');

        foreach ($configuredModels as $configuredModel) {
            $modelTemplate = new $configuredModel['class'];
            self::validateSettingModelKey();

            $response = $this->pullFromTranslationCaptain(self::getTranslationCaptainId($modelTemplate));

            $translations = [];
            if (array_key_exists('data', $response)) {
                if (array_key_exists('items', $response['data'])) {
                    $items = $this->encryptService->decrypt($response['data']['items']);
                    $items = json_decode($items, true);
                    $translations = $items['downloaded'];
                }
            }

            foreach ($translations as $id => $translation) {
                // force to string as we can have a int id or a string id (ie uuid)
                $this->updateTranslations($modelTemplate, (string)$id, $translation);
            }
        }

        return $this->return;
    }

    private function pullFromTranslationCaptain(string $modelId)
    {
        $domain = config('translationcaptain.url');
        $projectId = config('translationcaptain.project_id');
        $url = $domain . "/projects/" . $projectId . "/models/" . $modelId . "/download";

        try {
            $apiAccessToken = config('translationcaptain.api_key');
            $response = Http::withToken($apiAccessToken)->acceptJson()->get($url);
            self::addPullLogEntry($projectId, $response);
        } catch (\Exception $e) {
            self::addPullLogEntry($projectId, $response);
            throw new PullFailedException($e->getMessage());
        }

        return $response->json();
    }

    private function updateTranslations($modelTemplate, string $id, array $translations)
    {
        $idField = self::getClientModelId(new $modelTemplate());
        $find = $modelTemplate->where($idField, '=', $id)->first();

        if (!$find) {
            $message = "cannot find model with id: $id, in your database";
            throw new InvalidModelPullException($message);
        }

        foreach ($translations as $field => $languages) {
            if (
                $this->isTranslatable($find, $field) &&
                $this->hasAttribute($find, $field)
            ) {
                $find->setTranslations($field, $languages);

                $this->return[$find->id][$field] = $languages;
            } else {

                if ($this->hasCasedAttribute($find, $field)) {
                    $message = "
                        Fieldname in your TranslationCaptain Model definition does not match your model.
                        These should be exactly the same (case sensitive). The field ($field) mismatches the case in your model";
                    throw new ModelPullFailedFieldException($message);
                }
            }
        }

        $find->ignorePushOnSave = true;
        $find->save();
    }

    private function isTranslatable($model, $field)
    {
        return in_array($field, $model->getTranslatableAttributes());
    }

    private function hasCasedAttribute($model, $attr)
    {
        $fieldsAllLowerCase = array_map('strtolower', array_keys($model->getAttributes()));
        return in_array(strtolower($attr), $fieldsAllLowerCase);
    }

}
