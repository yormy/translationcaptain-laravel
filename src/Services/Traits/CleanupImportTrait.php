<?php

namespace Yormy\TranslationcaptainLaravel\Services\Traits;

use Yormy\TranslationcaptainLaravel\Exceptions\MaxKeyLengthException;

trait CleanupImportTrait
{
    protected static function truncateKeys(array $keyValues)
    {
        $maxKeyLength = 150;
        foreach ($keyValues as $key => $value) {
            if (is_string($key) && (strlen($key) > $maxKeyLength)) {

                if (config('translationcaptain.invalid_key.exception')) {
                    throw new MaxKeyLengthException($key);
                }

                $truncatedKey = substr($key, 0, $maxKeyLength);
                $truncatedKey .= md5($truncatedKey);
                $truncatedKey .= config('translationcaptain.invalid_key.autofix_postfix');

                $keyValues[$truncatedKey] = $value;

                unset($keyValues[$key]);
            }
        }

        return $keyValues;
    }
}
