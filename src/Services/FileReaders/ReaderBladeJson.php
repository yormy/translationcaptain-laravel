<?php declare(strict_types = 1);

namespace Yormy\TranslationcaptainLaravel\Services\FileReaders;

use Illuminate\Support\Arr;
use Yormy\TranslationcaptainLaravel\Exceptions\InvalidSetupException;
use Yormy\TranslationcaptainLaravel\Services\Traits\CleanupImportTrait;

class ReaderBladeJson extends ReaderBlade
{
    use CleanupImportTrait;

    protected function getExtension(): string
    {
        return ".json";
    }

    /**
     * Convert the php array structure text file into an php array object with dot notations
     */
    protected function convertImportfileToArray(string $filename): array
    {
        $contents = file_get_contents($filename);
        $arrayTranslations = json_decode($contents, true);

        try {
            $keyValues = Arr::dot($arrayTranslations);
        } catch (\Throwable $e) {
            throw new InvalidSetupException('Your json cannot end on a comma (,)');
        }

        $keyValues = $this->fixEmptyArray($keyValues);

        $keyValues = self::truncateKeys($keyValues);

        return $keyValues;
    }

}
