<?php declare(strict_types = 1);

namespace Yormy\TranslationcaptainLaravel\Services\FileReaders;

use Illuminate\Support\Arr;
use Yormy\TranslationcaptainLaravel\Services\Traits\CleanupImportTrait;

class ReaderBladePhp extends ReaderBlade
{
    use CleanupImportTrait;

    protected function getExtension(): string
    {
        return ".php";
    }

    /**
     * Convert the php array structure text file into an php array object with dot notations
     */
    protected function convertImportfileToArray(string $filename): array
    {
        $arrayTranslations = include $filename;

        $keyValues = Arr::dot($arrayTranslations);

        $keyValues = $this->fixEmptyArray($keyValues);

        $keyValues = self::truncateKeys($keyValues);

        return $keyValues;
    }
}
