<?php declare(strict_types = 1);

namespace Yormy\TranslationcaptainLaravel\Services\FileReaders;

use Illuminate\Support\Arr;

abstract class ReaderBlade extends FileReader
{

    /**
     * Published vendor translations
     * lang/vendor/<package-name>/<language>/directory/directory/translations.php
     */
    const VENDOR_FILES = 0;

    /**
     * App translations
     * lang/<language>/directory/directory/translations.php
     */
    const APP_FILES = 1;

    /**
     * App translations
     * lang/en.php
     */
    const APP_SINGLE_FILES = 2;

    public function __construct(array $locales)
    {
        $this->importPath = App()['path.lang'];

        $endingChars = "\.|;|:| |$|@|\(|\)";
        $this->dataBindingPattern = ":([a-zA-Z]+?)($endingChars)";

        parent::__construct($locales);
    }

    public function getMessages()
    {
        foreach ($this->locales as $locale) {
            $filename = $this->importPath . DIRECTORY_SEPARATOR . $locale . $this->getExtension();
            $this->addSingleTranslationFiles(self::APP_SINGLE_FILES, $filename, $this->importPath, $locale);
        }

        foreach ($this->locales as $locale) {
            $importFromLanguageDir = $this->importPath . DIRECTORY_SEPARATOR . $locale;
            $this->importFileTranslations(self::APP_FILES, $importFromLanguageDir, $importFromLanguageDir, $locale);
        }

        $importFromVendorDir = $this->importPath . "/vendor";
        $this->importFileTranslations(self::VENDOR_FILES, $importFromVendorDir, $importFromVendorDir, $locale);

        return $this->messages;
    }

    public function addSingleTranslationFiles(int $directoryType, string $fullPathname, string $root, string $language = null) : void
    {
        if (! is_file($fullPathname)) {
            return;
        }

        $relative = str_replace($root, '', $fullPathname);

        // remove php extension
        $relative = substr($relative, 0, strlen($relative) - strlen($this->getExtension()));

        // strip leading directory separator
        $first = substr($relative, 0, 1);
        if ($first === DIRECTORY_SEPARATOR) {
            $relative = substr($relative, 1, strlen($relative));
        }

        if (self::VENDOR_FILES === $directoryType) {
            $exploded = $this->explodeVendorDirectory($relative);
            $relative = $exploded['relative'];
            $language = $exploded['language'];
        }

        $keysForPackage = [];
        $translations = $this->convertImportfileToArray($fullPathname);
        foreach ($translations as $key => $translation) {
            $keysForPackage[$key] = $this->processTranslation($translation);
        }

        if (strcasecmp($relative, $language) === 0) {
            $relative = config('translationcaptain.group_when_group_missing');
        }

        if (!empty($keysForPackage)) {
            $this->messages[$language][$relative] = $keysForPackage;
        }
    }

    /**
     * Convert the php array structure text file into an php array object with dot notations
     */
    abstract protected function convertImportfileToArray(string $filename): array;

    protected function processTranslation(string $translation) : string
    {
        $translation = $this->createNewDataBinding($translation);

        return $translation;
    }

    protected function getRawDataBinding($value) : string
    {
        return ":$value";
    }
}
