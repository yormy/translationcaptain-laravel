<?php declare(strict_types = 1);

namespace Yormy\TranslationcaptainLaravel\Services\FileReaders;

use Illuminate\Support\Arr;
use Yormy\TranslationcaptainLaravel\Exceptions\InvalidTranslationFileException;
use Yormy\TranslationcaptainLaravel\Services\FileTypes\FileTypeJson;
use Yormy\TranslationcaptainLaravel\Services\JsonImport;

class ReaderVue extends FileReader
{
    const VENDOR_FILES = 0;

    /**
     * App translations
     * lang/<language>/directory/directory/translations.json
     */
    const APP_FILES = 1;

    public function __construct(array $locales)
    {
        $this->filetype = new FileTypeJson();

        $this->dataBindingPattern = "{(.*?)}";

        parent::__construct($locales);
    }

    protected function getExtension(): string
    {
        return ".json";
    }

    public function getMessages()
    {
        foreach ($this->locales as $locale) {
            $importFromLanguageDir = $this->importPath . DIRECTORY_SEPARATOR . $locale;
            $this->importFileTranslations(self::APP_FILES, $importFromLanguageDir, $importFromLanguageDir, $locale);
        }

        $importFromVendorDir = $this->importPath . "/vendor";
        $this->importFileTranslations(self::VENDOR_FILES, $importFromVendorDir, $importFromVendorDir, $locale);

        return $this->messages;
    }

    public function addSingleTranslationFiles(int $directoryType, string $fullPathname, string $root, string $language = null) : void
    {
        if (! is_file($fullPathname)) {
            return;
        }

        $relative = $this->getBareFilename($root, $fullPathname);

        $keysForPackage = array();
        $translations = JsonImport::convertImportfileToArray($fullPathname);

        if (count($translations) > 0) {
            foreach ($translations as $key => $translation) {
                $keysForPackage[$key] = $this->processTranslation($translation);
            }

            if (self::VENDOR_FILES === $directoryType) {
                $exploded = $this->explodeVendorDirectory($relative);
                $relative = $exploded['relative'];
                $language = $exploded['language'];
            }

            $this->messages[$language][$relative] = $keysForPackage;
        }
    }

    protected function processTranslation(string $translation) : string
    {
        $translation = $this->createNewDataBinding($translation);

        return $translation;
    }


    protected function getRawDataBinding($value) : string
    {
        return '{'. $value. '}';
    }
}
