<?php

namespace Yormy\TranslationcaptainLaravel\Services;

use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Translation\Translator as BaseTranslator;
use Yormy\TranslationcaptainLaravel\Exceptions\MissingTranslationException;
use Yormy\TranslationcaptainLaravel\Observers\Events\MissingTranslationEvent;

class BackupService
{
    protected string $disk;
    protected string $path;
    protected string $sourcePath;
    protected string $tag;

    private $zipFilenamePrefix = "tcbackup";

    public function __construct() {
        $this->disk = config('translationcaptain.backup.disk');
        $this->path = config('translationcaptain.backup.path');
    }

    public function backup(string $sourcePath, string $tag)
    {
        $this->tag = "of-$tag";
        $this->deleteOutdatedBackups();
        $this->zipping($sourcePath);
        $this->moveToDisc();
    }

    private function deleteOutdatedBackups()
    {
        $existingBackups = Storage::disk($this->disk)->files($this->path);

        if(sizeof($existingBackups) >= config('translationcaptain.backup.keep_number_backups')) {
            Storage::disk($this->disk)->delete($existingBackups[0]);
        }
    }

    private function sluggify($filename)
    {
        $filename = str_replace(DIRECTORY_SEPARATOR, "_", $filename);
        return Str::slug($filename);
    }

    private function generateFilename()
    {
        $timestamp = date('Y-m-d_H-i');
        $zipFilename = $this->zipFilenamePrefix. "-". $this->tag. "-". "@$timestamp";

        $zipFilename = $this->sluggify($zipFilename).".zip";
        return $zipFilename;
    }

    private function generateTempFilepath()
    {
        $zipFilename = $this->generateFilename();

        $storagePath = storage_path(). DIRECTORY_SEPARATOR. 'temp';

        if (!file_exists($storagePath)) {
            mkdir($storagePath, 0666, true);
        }

        return $storagePath. DIRECTORY_SEPARATOR. $zipFilename;
    }

    private function moveToDisc()
    {
        $zipTempPath = $this->generateTempFilepath();

        $zipFilename = $this->generateFilename();
        $zipDiskLocation = $this->path. DIRECTORY_SEPARATOR. $zipFilename;

        if (file_exists($zipTempPath)) {
            Storage::disk($this->disk)->put($zipDiskLocation, file_get_contents($zipTempPath));
            unlink($zipTempPath);
        }
    }

    private function zipping(string $sourcePath)
    {
        $zipTempPath = $this->generateTempFilepath();

        $zip = new \ZipArchive();
        $zip->open($zipTempPath, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($sourcePath));
        foreach ($files as $file) {
            // We're skipping all subfolders
            if (! $file->isDir()) {
                $filePath = $file->getRealPath();

                $relativePath = '' . substr($filePath, strlen($sourcePath) + 1);

                $zip->addFile($filePath, $relativePath);
            }
        }
        $zip->close();
    }
}
