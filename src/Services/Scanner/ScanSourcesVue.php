<?php declare(strict_types=1);

namespace Yormy\TranslationcaptainLaravel\Services\Scanner;

use function config;

class ScanSourcesVue extends ScanSourcesBase
{
    /**
     * Translation function pattern.
     *
     * @var string
     */
    protected string $pattern = '/(\$t)\([\'"](.+)[\'"][\),]/U';

    protected function getSourcePaths()
    {
        return config('translationcaptain.source_code_scan.vue.paths');
    }
}
