<?php declare(strict_types=1);

namespace Yormy\TranslationcaptainLaravel\Services\Scanner;

use function config;

class ScanSourcesBlade extends ScanSourcesBase
{
    /**
     * Translation function pattern.
     *
     * @var string
     */
    protected string $pattern = '/(__)\([\'"](.+)[\'"][\),]/U';

    protected function getSourcePaths()
    {
        return config('translationcaptain.source_code_scan.blade.paths');
    }
}
