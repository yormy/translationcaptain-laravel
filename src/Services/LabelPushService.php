<?php declare(strict_types=1);

namespace Yormy\TranslationcaptainLaravel\Services;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Yormy\TranslationcaptainLaravel\Exceptions\DuplicateKeyException;
use Yormy\TranslationcaptainLaravel\Exceptions\PushFailedException;
use Yormy\TranslationcaptainLaravel\Observers\Events\PushFailedEvent;
use Yormy\TranslationcaptainLaravel\Services\FileReaders\FileReader;
use Yormy\TranslationcaptainLaravel\Services\Scanner\ScanSourcesBlade;
use Yormy\TranslationcaptainLaravel\Services\Scanner\ScanSourcesVue;
use Yormy\TranslationcaptainLaravel\Traits\ProjectPushPullTrait;

class LabelPushService
{
    use ProjectPushPullTrait;

    protected $locales;

    protected $readers;

    protected $encryptService;

    public function __construct(array $locales)
    {
        $this->locales = $locales;

        foreach (config('translationcaptain.readers') as $readerConfig) {
            $reader = new $readerConfig['reader']($locales);
            $reader->setImportPath(base_path() . $readerConfig['import_path']);
            $this->addReader($reader);
        }

        $this->encryptService = new EncryptService(config('translationcaptain.encryption_key'));
    }

    public function addReader(FileReader $reader)
    {
        $this->readers[] = $reader;
    }

    public function pushLabelsToRemote()
    {
        $cacheService = new CacheService();

        $allKeys = $this->getAllKeys();
        $newKeys = $cacheService->getNewKeys($allKeys);

        $domain = config('translationcaptain.url');
        $projectId = config('translationcaptain.project_id');
        $url = $domain . "/projects/" . $projectId . "/labels/upload";

        $data = [
            'translations' => $this->encryptService->encrypt(base64_encode(json_encode($newKeys))),
            'base_locale' => config('translationcaptain.default_locale'),
        ];

        try {
            $apiAccessToken = config('translationcaptain.api_key');
            $response = Http::withToken($apiAccessToken)->acceptJson()->post($url, $data);

            self::addPushLogEntry($projectId, $response);
        } catch (\Exception $e) {
            self::addPushLogEntry($projectId, null);
            throw new PushFailedException($e->getMessage());
        }

        if ($response->serverError()) {
            throw new PushFailedException('Server error');
        }

        if ($response->Ok()) {
            $this->deleteQueue();

            $cacheService = new CacheService();
            $cacheService->updatePushedKeys($newKeys);

            return $response;
        }

        event(new PushFailedEvent($response));

        return $response;
    }

    public function getAllKeys()
    {
        $existingTranslations = $this->getExistingTranslations();

        if (config('translationcaptain.source_code_scan.blade.enabled')) {
            $importer = new ScanSourcesBlade();
            $foundKeys = $importer->getMessages();
            $missingKeysFromSourceFiles = $this->getMissingKeysFromSourceFiles($existingTranslations, $foundKeys);
            $existingTranslations = $this->mergeLabels($existingTranslations, $missingKeysFromSourceFiles);
        }

        if (config('translationcaptain.source_code_scan.vue.enabled')) {
            $importer = new ScanSourcesVue();
            $foundKeys = $importer->getMessages();
            $missingKeysFromSourceFiles = $this->getMissingKeysFromSourceFiles($existingTranslations, $foundKeys);
            $existingTranslations = $this->mergeLabels($existingTranslations, $missingKeysFromSourceFiles);
        }

        return $existingTranslations;
    }

    private function getExistingTranslations()
    {
        $labels = [];
        foreach ($this->readers as $reader) {
            $readLabels = $reader->getMessages();
            $labels = $this->mergeLabels($labels, $readLabels);
        }

        return $labels;
    }

    private function mergeLabels(array $origin, array $toMerge): array
    {
        $this->checkMerge($origin, $toMerge);

        return array_replace_recursive($origin, $toMerge);
    }

    public function checkMerge(array $labels, array $labelsToMerge)
    {
        $labelsDotted = Arr::dot($labels);
        $labelsToMergeDotted = Arr::dot($labelsToMerge);

        foreach ($labelsDotted as $key => $translation) {
            if (array_key_exists($key, $labelsToMergeDotted)) {
                $labelTranslation = $this->removeBinding($translation);
                $labelTranslationToMerge = $this->removeBinding($labelsToMergeDotted[$key]);

                if ($labelTranslation !== $labelTranslationToMerge) {
                    throw new DuplicateKeyException($key, $labelTranslation, $labelTranslationToMerge);
                }
            }
        }
    }

    public function removeBinding(string $translation): string
    {
        $start = config('translationcaptain.databinding.start');
        $end = config('translationcaptain.databinding.end');
        $pattern = "$start(.*?)$end";

        return preg_replace("/" . $pattern . "/", '', $translation);
    }

    private function getMissingKeysFromSourceFiles(array $existingTranslations, array $foundKeys): array
    {
        $foundKeysDotted = Arr::dot($foundKeys);

        $missingKeys = [];
        foreach ($this->locales as $locale) {
            $missingKeysForLanguage = [];

            if (array_key_exists($locale, $existingTranslations)) {
                $existingForLanguageDotted = Arr::dot($existingTranslations[$locale]);

                foreach (array_keys($foundKeysDotted) as $key) {
                    if (!array_key_exists($key, $existingForLanguageDotted)) {
                        $missingKeysForLanguage = $this->addMissingKey($key, $missingKeysForLanguage);
                    }
                }
            } else {
                foreach (array_keys($foundKeysDotted) as $key) {
                    $missingKeysForLanguage = $this->addMissingKey($key, $missingKeysForLanguage);
                }
            }

            $missingKeys[$locale] = $missingKeysForLanguage;
        }

        return $missingKeys;
    }

    private function addMissingKey(string $fullKey, array $missingKeys): array
    {
        $firstDot = strpos($fullKey, '.');
        if ($firstDot === false || $firstDot === 0) {
            return $missingKeys;
        }

        $group = substr($fullKey, 0, $firstDot);
        $key = substr($fullKey, $firstDot + 1, strlen($fullKey));

        $missingKeys[$group][$key] = "#$group.$key";

        return $missingKeys;
    }

    private function deleteQueue()
    {
        $queueFilename = config('translationcaptain.missing_translation.queue_filename');
        Storage::delete($queueFilename);
    }
}
