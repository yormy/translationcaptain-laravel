<?php declare(strict_types = 1);

namespace Yormy\TranslationcaptainLaravel\Services;

use Illuminate\Encryption\Encrypter;

class EncryptService
{
    private Encrypter $encrypter;

    public function __construct(string $encryptionKey)
    {
        $encryptionKeyDecoded = base64_decode(str_replace('base64:','', $encryptionKey));

        try {
            $this->encrypter = new Encrypter($encryptionKeyDecoded, config('translationcaptain.cipher'));
        } catch (\Exception $e) {
            echo "Invalid encryption key in your config (not appropriate for selected cipher)";
            die();
        }
    }

    public function encrypt(string $plainText): string
    {
        return $this->encrypter->encryptString($plainText);
    }

    public function decrypt(string $cipherText): string
    {
        try {
            return $this->encrypter->decryptString($cipherText);
        } catch (\Exception $e) {
            echo "Invalid encryption key in your config. Please copy it from your project in TranslationCaptain";
            die();
        }
    }
}
