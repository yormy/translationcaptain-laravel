<?php

namespace Yormy\TranslationcaptainLaravel\Services;

use Illuminate\Support\Arr;
use Yormy\TranslationcaptainLaravel\Exceptions\InvalidTranslationFileException;
use Yormy\TranslationcaptainLaravel\Services\Traits\CleanupImportTrait;

class JsonImport
{
    use CleanupImportTrait;

    public static function convertImportfileToArray(string $filename): array
    {
        $fileExtension = pathinfo($filename)['extension'];
        if (strtoupper($fileExtension) !== 'JSON') {
            return [];
        }

        if (!static::isJson(file_get_contents($filename))) {
            throw new InvalidTranslationFileException($filename);
        }

        $arrayTranslations = json_decode(file_get_contents($filename), true);

        if (is_array($arrayTranslations)) {
            $keyValues = Arr::dot($arrayTranslations);
            $keyValues = static::fixEmptyArray($keyValues);

            $keyValues = self::truncateKeys($keyValues);

            return $keyValues;
        }

        return [];
    }

    protected static function fixEmptyArray(array $keyValues): array
    {
        // Arr::dot convert an empty array not to a dotted value but remains an empty array.
        // Remove this empty array so we can trust on a single dimensional array
        foreach ($keyValues as $key => $value) {
            if (is_array($value)) {
                unset($keyValues[$key]);
            }
        }

        return $keyValues;
    }

    private static function isJson($string): bool
    {
        /**
         * @psalm-suppress UnusedFunctionCall
         */
        json_decode($string);   // to get the errors

        return (json_last_error() == JSON_ERROR_NONE);
    }
}
