<?php

namespace Yormy\TranslationcaptainLaravel\Services\FileWriters;

class CleanOutput
{
    public static function cleanKey(string $value): string
    {
        $value = static::clean($value);

        return $value;
    }

    public static function cleanValue(string $value): string
    {
        $value = static::clean($value);

        return $value;
    }

    private static function clean(string $value): string
    {
        $value = str_replace("\n", "\\n", $value);
        $value = str_replace("\\n", "\\n", $value);

        $value = str_replace("\r", "\\r", $value);
        $value = str_replace("\\r", "\\r", $value);

        $value = str_replace("\\'", "'", $value);
        $value = str_replace("'", "''", $value);
        $value = str_replace("\"", "''", $value);
        $value = str_replace('\\', "\\\\", $value);

        return $value;
    }
}
