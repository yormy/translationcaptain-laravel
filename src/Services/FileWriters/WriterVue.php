<?php declare(strict_types=1);

namespace Yormy\TranslationcaptainLaravel\Services\FileWriters;

use Yormy\TranslationcaptainLaravel\Exceptions\InvalidOutputException;
use Yormy\TranslationcaptainLaravel\Services\FileTypes\FileTypeJson;
use Yormy\TranslationcaptainLaravel\Services\JsonImport;

class WriterVue extends FileWriter
{
    protected $vendorPath = 'vendor';

    public function __construct()
    {
        $this->exportSettings = new ExportSettingsJson();

        $this->filetype = new FileTypeJson();

        $this->fullExportPath = App()['path.lang'];
        $this->fullExportPath .= '_tc_vuw';

        parent::__construct();
    }

    protected function prepareTranslationForExport(string $translation): string
    {
        return parent::prepareTranslationForExport($translation);
    }

    protected function processMessage(string $message): string
    {
        $jsonMessage = json_encode($message);
        $jsonMessage = $this->processDataBinding($jsonMessage);

        return json_decode($jsonMessage, true);
    }

    protected function makeRawDataBinding($value): string
    {
        return '{' . $value . '}';
    }

    protected function checkContentOutput(string $fileContents, string $fullpath)
    {
        $isValidJson = json_decode($fileContents, true); // this throws an exception when contents is invalid
        if (!$isValidJson) {
            throw new InvalidOutputException($fullpath);
        }
    }

    protected function mergeWriteWithExisting(array $filesToExport): array
    {
        foreach ($filesToExport as $filename => $content) {
            $fullpath = $this->fullExportPath . DIRECTORY_SEPARATOR . $filename;

            $existing = array();
            if (file_exists($fullpath)) {
                $existing = JsonImport::convertImportfileToArray($fullpath);
                unset($existing['##' . $this->header]); // remove header branding to prevent duplicates
            }

            $translations = array_merge($existing, $content);

            $filesToExport[$filename] = $translations;
        }

        return $filesToExport;
    }

}
