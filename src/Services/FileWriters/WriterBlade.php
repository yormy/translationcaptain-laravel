<?php declare(strict_types = 1);

namespace Yormy\TranslationcaptainLaravel\Services\FileWriters;

use Yormy\TranslationcaptainLaravel\Services\FileTypes\FileTypePhp;

class WriterBlade extends FileWriter
{
    protected $vendorPath = 'vendor';

    public function __construct()
    {
        $this->exportSettings = new ExportSettingsPhp();

        $this->filetype = new FileTypePhp();

        $this->fullExportPath = App()['path.lang'];
        $this->fullExportPath .= '_tc';

        parent::__construct();
    }

    protected function groupnameToFilename(string $groupName, string $locale): string
    {
        if ($groupName == config('translationcaptain.group_when_group_missing')) {
            return $locale. $this->filetype->extension; // write as en.php
        }

        return parent::groupnameToFilename($groupName, $locale);
    }

    protected function processMessage(string $message) : string
    {
        return $this->processDataBinding($message);
    }

    protected function makeRawDataBinding($value) : string
    {
        return ':'. $value;
    }

    protected function checkContentOutput(string $fileContents, string $fullpath)
    {
        //
    }

    protected function mergeWriteWithExisting(array $filesToExport): array
    {
        foreach ($filesToExport as $filename => $content) {

            $fullpath = $this->fullExportPath . DIRECTORY_SEPARATOR . $filename;

            $existing = array();
            if (file_exists($fullpath)) {
                $existing = include $fullpath;
            }

            $translations = array_merge($existing, $content);

            $filesToExport[$filename] = $translations;
        }

        return $filesToExport;
    }
}

