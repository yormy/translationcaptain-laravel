<?php declare(strict_types=1);

namespace Yormy\TranslationcaptainLaravel\Services;

use Illuminate\Support\Facades\Http;
use Yormy\TranslationcaptainLaravel\Exceptions\PullFailedException;
use Yormy\TranslationcaptainLaravel\Observers\Events\PullFailedEvent;
use Yormy\TranslationcaptainLaravel\Services\FileWriters\FileWriter;
use Yormy\TranslationcaptainLaravel\Traits\ProjectPushPullTrait;

class LabelPullService
{
    use ProjectPushPullTrait;

    private $writers;

    protected $encryptService;

    public function __construct()
    {
        foreach (config('translationcaptain.writers') as $writerConfig) {
            $writer = new $writerConfig['writer']();
            $writer->setExportPath(base_path(). $writerConfig['export_path']);
            $this->addWriter($writer);

            $this->encryptService = new EncryptService(config('translationcaptain.encryption_key'));
        }
    }

    public function addWriter(FileWriter $writer)
    {
        $this->writers[] = $writer;
    }

    public function pullFromRemote()
    {
        $cacheService = new CacheService();
        $timestamp = $cacheService->readTimestamp();

        $domain = config('translationcaptain.url');
        $projectId = config('translationcaptain.project_id');
        $url = $domain . "/projects/" . $projectId . "/labels/download";

        $locales = implode(",", config('translationcaptain.locales'));
        $url .= "?locales=$locales";
        $url .= "&since=$timestamp";

        try {
            $apiAccessToken = config('translationcaptain.api_key');
            $response = Http::withToken($apiAccessToken)->acceptJson()->get($url);

            self::addPullLogEntry($projectId, $response);
        } catch (\Exception $e) {
            self::addPullLogEntry($projectId, null);

            throw new PullFailedException($e->getMessage());
        }

        if ($response->serverError()) {
            throw new PullFailedException('Server error');
        }

        if ($response->Ok()) {
            $decryptedData = $this->encryptService->decrypt($response->json()['data']);
            $data = json_decode($decryptedData, true);

            $cacheService->writeTimestamp($data['timestamp']);
            $pulledKeys = $data['items']['downloaded'];
            $this->generateFiles($pulledKeys, $timestamp);

            return $pulledKeys;
        }

        event(new PullFailedEvent($response));

        return $response->json();
    }

    private function generateFiles(array $pulledKeys, int $sinceTimestamp): void
    {
        foreach ($this->writers as $writer) {
            $writer->setLabels($pulledKeys);
            $writer->export($sinceTimestamp);
        }
    }
}
