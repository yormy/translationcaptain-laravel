<?php declare(strict_types=1);

namespace Yormy\TranslationcaptainLaravel\Services;

use Illuminate\Support\Facades\Http;
use Yormy\TranslationcaptainLaravel\Exceptions\PushFailedException;
use Yormy\TranslationcaptainLaravel\Traits\ModelValidationTrait;
use Yormy\TranslationcaptainLaravel\Traits\ProjectPushPullTrait;

class ModelPushService
{
    use ModelValidationTrait;
    use ProjectPushPullTrait;

    protected $encryptService;

    public function __construct()
    {
        $this->encryptService = new EncryptService(config('translationcaptain.encryption_key'));
    }

    public function buildData(?string $baseLanguage = null)
    {
        $models = config('translationcaptain.models');

        foreach ($models as $model) {
            $modelTemplate = new $model['class'];
            self::validateSettingModelKey();

            $translations = $modelTemplate->all();

            $upload = [];

            foreach ($translations as $translation) {

                $translatables = $translation->getTranslatableAttributes();

                foreach ($translatables as $fieldname) {
                    self::validateFieldName($translation, $fieldname);

                    $idField = self::getClientModelId(new $modelTemplate());
                    $modelId = $model['translationcaptain_id'];

                    if (!$baseLanguage) {
                        $languages = $translation->getTranslations($fieldname);
                    } else {
                        $languages[$baseLanguage] = $translation->getTranslation($fieldname, $baseLanguage);
                    }

                    $upload[$modelId][$translation->$idField][$fieldname] = $languages;
                }
            }
        }

        return $upload;
    }

    public function pushModelToRemote(array $translatables, string $modelId, string $clientModelId)
    {
        $domain = config('translationcaptain.url');
        $projectId = config('translationcaptain.project_id');
        $url = $domain . "/projects/" . $projectId;
        $url .= "/models/$modelId";
        $url .= "/$clientModelId";
        $url .= "/upload";

        $data['translatables'] = $this->encryptService->encrypt(json_encode($translatables));

        try {
            $apiAccessToken = config('translationcaptain.api_key');
            $response = Http::withToken($apiAccessToken)->acceptJson()->post($url, $data);

            $projectId = config('translationcaptain.project_id');
            self::addPushLogEntry($projectId, $response);

        } catch (\Exception $e) {
            self::addPushLogEntry($projectId, null);
            throw new PushFailedException($e->getMessage());
        }

        return $response;
    }

    public function pushAllModelsToRemote(array $data, ?string $i18n = null)
    {
        $domain = config('translationcaptain.url');
        $projectId = config('translationcaptain.project_id');
        $url = $domain . "/projects/" . $projectId;
        $url .= "/models";
        $url .= "/upload";

        if ($i18n) {
            $url .= "/$i18n";
        }

        $data['all'] = $this->encryptService->encrypt(json_encode($data));

        try {
            $apiAccessToken = config('translationcaptain.api_key');
            $response = Http::withToken($apiAccessToken)->acceptJson()->post($url, $data);
            self::addPushLogEntry($projectId, $response);
            return $response->json();
        } catch (\Exception $e) {
            self::addPushLogEntry($projectId, null);
            throw new PushFailedException($e->getMessage());
        }
    }
}
