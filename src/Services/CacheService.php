<?php

namespace Yormy\TranslationcaptainLaravel\Services;

use Exception;
use Yormy\TranslationcaptainLaravel\Helpers\ArrayHelper;

class CacheService
{
    private string $cacheFile;

    private string $timestampFile;

    private array $cachedKeys = [];

    public function __construct()
    {
        $this->timestampFile = storage_path('logs/translationcaptain.timestamp');
        $this->cacheFile = storage_path('logs/translationcaptain.cache');
    }

    public function getNewKeys(array $allFoundKeys): array
    {
        $pushedKeys = $this->readCache();

        return ArrayHelper::array_diff_keys_assoc_recursive($allFoundKeys, $pushedKeys);
    }

    public function updatePushedKeys(array $newKeys)
    {
        $pushedKeys = $this->readCache();

        $newKeys = array_merge($pushedKeys, $newKeys);

        $this->writeCache($newKeys);
    }

    public function writeTimestamp(int $timestamp)
    {
        file_put_contents($this->timestampFile, (string)$timestamp );
    }

    public function writeCache(array $keys)
    {
        file_put_contents($this->cacheFile, json_encode($keys));
    }

    public function readTimestamp(): int
    {
        if (!file_exists($this->timestampFile)) {
            return 0;
        }

        return (int)file_get_contents($this->timestampFile);
    }

    public function readCache(): array
    {
        if (!$this->cachedKeys && file_exists($this->cacheFile)) {
            $contents = file_get_contents($this->cacheFile);
            $this->cachedKeys = json_decode($contents, true);
        }

        return $this->cachedKeys;
    }

    private function getEncryptedResult($data)
    {
        $encryptService = new EncryptService(config('translationcaptain.encryption_key'));

        $responseData = $data;
        try {
            if (!is_array($data)) {
                $responseData = json_decode($encryptService->decrypt($data), true);
            }
        } catch (Exception $e) {
            $responseData = json_decode($data, true);
        }

        return $responseData;
    }

}
