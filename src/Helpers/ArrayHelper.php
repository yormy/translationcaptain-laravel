<?php

namespace Yormy\TranslationcaptainLaravel\Helpers;

class ArrayHelper
{
    public static function array_diff_assoc_recursive($array1, $array2) {
        $difference=array();
        foreach($array1 as $key => $value) {
            if( is_array($value) ) {
                if( !isset($array2[$key]) || !is_array($array2[$key]) ) {
                    $difference[$key] = $value;
                } else {
                    $new_diff = self::array_diff_assoc_recursive($value, $array2[$key]);
                    if( !empty($new_diff) )
                        $difference[$key] = $new_diff;
                }
            } else if( !array_key_exists($key,$array2) || $array2[$key] !== $value ) {
                $difference[$key] = $value;
            }
        }
        return $difference;
    }

    public static function array_diff_keys_assoc_recursive($array1, $array2) {
        $difference=array();
        foreach($array1 as $key => $value) {
            if( is_array($value) ) {
                if( !isset($array2[$key]) || !is_array($array2[$key]) ) {
                    $difference[$key] = $value;
                } else {
                    $new_diff = self::array_diff_keys_assoc_recursive($value, $array2[$key]);
                    if( !empty($new_diff) )
                        $difference[$key] = $new_diff;
                }
            } else if( !array_key_exists($key,$array2)) {
                $difference[$key] = $value;
            }
        }
        return $difference;
    }


//    protected function sanitizeKeysRecursive(array &$keyArray): array
//    {
////        foreach ($keyArray as $key => $value) {
////            if (is_array($value)) {
////                $this->sanitizeKeysRecursive( $keyArray[ $key ] );
////            } elseif (false !== strpos($key, '.')) {
////                if (config('translationcaptain.invalid_key.exception')) {
////                    throw new InvalidKeyException($key);
////                }
////
////                $cleanedKey = str_replace('.', "_", $key);
////                $cleanedKey .= config('translationcaptain.invalid_key.autofix_postfix'); //??
////                unset($keyArray[$key]);
////                $keyArray[$cleanedKey] = $value;
////            }
////        }
//
//        return $keyArray;
//    }
}
