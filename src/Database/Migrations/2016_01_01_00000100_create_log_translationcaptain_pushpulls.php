<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogTranslationcaptainPushpulls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_translationcaptain_pushpulls', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('project_id');
            $table->string('type');
            $table->integer('pushed')->nullable();
            $table->integer('pulled')->nullable();
            $table->json('result')->nullable();
            $table->json('data');
            $table->timestamps();
        });
    }
}
