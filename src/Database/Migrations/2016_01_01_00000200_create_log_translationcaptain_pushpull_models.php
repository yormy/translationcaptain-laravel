<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogTranslationcaptainPushpullModels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_translationcaptain_pushpull_models', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('project_id');
            $table->string('model_tyoe');
            $table->string('model_id');
            $table->json('result')->nullable();
            $table->json('data');
            $table->timestamps();
        });
    }
}
