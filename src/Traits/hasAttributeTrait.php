<?php

namespace Yormy\TranslationcaptainLaravel\Traits;

trait hasAttributeTrait
{
    private static function hasAttribute($model, $attr)
    {
        return array_key_exists($attr, $model->getAttributes());
    }
}
