<?php

namespace Yormy\TranslationcaptainLaravel\Traits;

use Yormy\TranslationcaptainLaravel\Repositories\LogProjectPushPullRepository;
use Yormy\TranslationcaptainLaravel\Services\EncryptService;

trait ProjectPushPullTrait
{
    private static function addPullLogEntry(string $projectId, $response): void
    {
        $logProjectPushPullRepository = new LogProjectPushPullRepository();

        $type = '';
        $total = 0;

        if (!$response || !$responseArray = $response->json()) {
            $logProjectPushPullRepository->addPull($projectId, $type, [], $total, []);
            return;
        }

        $responseData = [];
        $responseResult = $response->json();

        if (array_key_exists('data', $responseArray)) {
            $responseData = self::getEncryptedResult($responseArray['data']);

            if (array_key_exists('count', $responseData)) {
                $total = $responseData['count']['total_processed'];
            }

            if (array_key_exists('type', $responseData)) {
                $type = $responseData['type'];
            }

            unset($responseArray['data']);
            $responseResult = $responseArray;
        }

        $logProjectPushPullRepository->addPull($projectId, $type, $responseData, $total, $responseResult);
    }

    private static function addPushLogEntry(string $projectId, $response): void
    {
        $logProjectPushPullRepository = new LogProjectPushPullRepository();

        $type = '';
        $total = 0;

        if (!$response || !$responseArray = $response->json()) {
            $logProjectPushPullRepository->addPush($projectId, $type, [], $total, []);
            return;
        }


        $responseData = [];
        $responseResult = $response->json();

        if (array_key_exists('data', $responseArray)) {
            $responseData = self::getEncryptedResult($responseArray['data']);

            if (array_key_exists('count', $responseData)) {
                $total = $responseData['count']['total_processed'];
            }

            if (array_key_exists('type', $responseData)) {
                $type = $responseData['type'];
            }

            unset($responseArray['data']);
            $responseResult = $responseArray;
        }

        $logProjectPushPullRepository->addPush($projectId, $type, $responseData, $total, $responseResult);
    }


    private static function getEncryptedResult($data)
    {
        $encryptService = new EncryptService(config('translationcaptain.encryption_key'));

        $responseData = $data;
        try {
            if (!is_array($data)) {
                $responseData = json_decode($encryptService->decrypt($data), true);
            }
        } catch (\Exception $e) {
            $responseData = json_decode($data, true);
        }

        return $responseData;
    }
}
