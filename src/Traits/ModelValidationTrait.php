<?php

namespace Yormy\TranslationcaptainLaravel\Traits;

use Illuminate\Support\Arr;
use Throwable;
use Yormy\TranslationcaptainLaravel\Exceptions\InvalidSetupException;

trait ModelValidationTrait
{
    use hasAttributeTrait;

    /** @psalm-suppress UnusedVariable */
    private static function validateSettingModelKey()
    {
        $configuredModels = config('translationcaptain.models');
        foreach ($configuredModels as $key => $configuredModel) {
            if (!Arr::get($configuredModel, 'class')) {
                throw new InvalidSetupException("Invalid model class for $key, specify the full class that you want to sync");
            }

            Try {
                $modelInstance = new $configuredModel['class'];
            } catch (Throwable $e) {
                throw new InvalidSetupException("Cannot create class class for $key");
            }

            if (!Arr::get($configuredModel, 'translationcaptain_id')) {
                throw new InvalidSetupException("Invalid translationcaptain_id for $key, get this key from your project model setup in TranslationCaptain");
            }
        }
    }

    private static function validateSettingPrimaryKey($model)
    {
        if (!self::getClientModelId($model)) {
            throw new InvalidSetupException("You must specify you primary key (id_field) in your model definition in your translationcaptain config");
        }
    }

    private static function validateFieldName($model, $fieldName)
    {
        if (!self::hasAttribute($model, $fieldName)) {
            throw new InvalidSetupException("The fieldname of your translatable ($fieldName) does not exist in your model");
        }
    }


    private static function getClientModelId($model)
    {
        return self::getTranslationCaptainModelConfig($model, 'id_field');
    }


    private static function getTranslationCaptainId($model): ?string
    {
        return self::getTranslationCaptainModelConfig($model, 'translationcaptain_id');
    }


    private static function getTranslationCaptainModelConfig($model, $field): ?string
    {
        $configuredModels = config('translationcaptain.models');

        foreach ($configuredModels as $configuredModel) {
            if (Arr::get($configuredModel, 'class') === get_class($model)) {
                return Arr::get($configuredModel, $field);
            }
        }

        return null;
    }
}
