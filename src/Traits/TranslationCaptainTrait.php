<?php

namespace Yormy\TranslationcaptainLaravel\Traits;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Yormy\TranslationcaptainLaravel\Exceptions\InvalidSetupException;
use Yormy\TranslationcaptainLaravel\Services\ModelPushService;

trait TranslationCaptainTrait
{
    use ModelValidationTrait;
    use hasAttributeTrait;
    use ProjectPushPullTrait;

    public $ignorePushOnSave = false;

    public static function bootTranslationCaptainTrait()
    {
        static::created(function ($model) {
            Log::debug('client:created tc model');
        });

        static::updated(function ($model) {
            Log::debug('client:updated tc model');
        });

        static::saved(function ($model) {
            // Prevent pushing again to TranslationsCaptain when pulling and updating from TranslationCaptain
            if ($model->ignorePushOnSave) {
                return;
            }

            $modelPushService = new ModelPushService();
            $translatableData = self::getTranslatableData($model);

            self::validateSettingPrimaryKey($model);
            self::validateSettingModelKey();

            $response = $modelPushService->pushModelToRemote(
                $translatableData,
                self::getTranslationCaptainId($model),
                self::getClientModelId($model));
        });
    }

    /**
     * Get the values of the translatable field names
     */
    private static function getTranslatableData($model): array
    {
        $data = [];

        foreach ($model->translatable as $translatableField) {
            if (self::hasAttribute($model, $translatableField)) {
                $data[$translatableField] = $model->$translatableField;
            }
        }

        return $data;
    }
}
