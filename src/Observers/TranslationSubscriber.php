<?php

namespace Yormy\TranslationcaptainLaravel\Observers;

use Illuminate\Events\Dispatcher;
use Yormy\TranslationcaptainLaravel\Observers\Events\MissingTranslationEvent;
use Yormy\TranslationcaptainLaravel\Observers\Events\PullFailedEvent;
use Yormy\TranslationcaptainLaravel\Observers\Events\PushFailedEvent;
use Yormy\TranslationcaptainLaravel\Observers\Listeners\MissingTranslationListener;
use Yormy\TranslationcaptainLaravel\Observers\Listeners\PullFailedListener;
use Yormy\TranslationcaptainLaravel\Observers\Listeners\PushFailedListener;

class TranslationSubscriber
{
    public function subscribe(Dispatcher $events)
    {
        $events->listen(
            MissingTranslationEvent::class,
            MissingTranslationListener::class
        );

        $events->listen(
            PushFailedEvent::class,
            PushFailedListener::class
        );

        $events->listen(
            PullFailedEvent::class,
            PullFailedListener::class
        );
    }
}
