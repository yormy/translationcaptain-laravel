<?php

namespace Yormy\TranslationcaptainLaravel\Observers\Listeners;

use Illuminate\Support\Facades\Log;
use Yormy\TranslationcaptainLaravel\Observers\Events\PushFailedEvent;

class PushFailedListener
{
    public function handle(PushFailedEvent $event)
    {
        $response = $event->getResponse();
        Log::Debug('TranslationCaptain: Push Failed : ', [$response->json()]);
    }
}
