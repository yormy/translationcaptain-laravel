<?php

namespace Yormy\TranslationcaptainLaravel\Observers\Listeners;

use Illuminate\Support\Facades\Log;
use Yormy\TranslationcaptainLaravel\Observers\Events\PullFailedEvent;

class PullFailedListener
{
    public function handle(PullFailedEvent $event)
    {
        $response = $event->getResponse();
        Log::Debug('TranslationCaptain: Pull Failed : ', [$response->json()]);
    }
}
