<?php

namespace Yormy\TranslationcaptainLaravel\Observers\Events;

class PushFailedEvent
{
    protected $response;

    public function __construct($response)
    {
        $this->response = $response;
    }

    public function getResponse()
    {
        return $this->response;
    }
}
